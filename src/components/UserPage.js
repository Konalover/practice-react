import AddUser from "../containers/AddUser";
import UsersList from "./UsersList";
import React from 'react';

const UserPage = () => (
    <div>
        <AddUser/>
        <UsersList/>
    </div>
);

export default UserPage;
