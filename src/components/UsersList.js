import React from 'react'
import User from './User'
import {connect} from "react-redux";

const UsersList = ({users}) => (
    <ul>
        {users.map((user, index) => (
            <User key={index} {...user}/>
        ))}
    </ul>
);

const mapStateToProps = state => {
    return {
        users: state.users
    }
};

export default connect(
    mapStateToProps
)(UsersList);
