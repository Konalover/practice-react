let nextUserId = 0;

export const addUser = name => {
    return {
        type: 'ADD_USER',
        id: nextUserId++,
        name
    }
};
